# language: pl

@all @login
Potrzeba biznesowa: Użytkownik powinien móc się zalogować
  Aby korzystać z systemu
  As a Użytkownik aplikacji
  Chciałbym mieć dostęp do aplikacji

  Założenia: Użytkownik powinien być wylogowany a strona powinna być w języku polskim i w trybie produkcyjnym
    Zakładając że odwiedzę stronę "/logout"
    I odwiedzę stronę "/locale/switch/pl"

  @first @javascript
  Scenariusz: Próba otwarcia aplikacji przez niezarejestrowanego użytkownika
    Zakładając że odwiedzę stronę "/profile/"
    Wtedy powinienem być na stronie "/login"
    Wtedy kod statusu odpowiedzi powinien być równy 200

  @second @javascript
  Scenariusz: Próba otwarcia aplikacji przez niezarejestrowanego użytkownika
    Zakładając że odwiedzę stronę "/profile/"
    Wtedy powinienem być na stronie "/login"
    Wtedy kod statusu odpowiedzi powinien być równy 200

  @thirdx
  Szablon scenariusza: Próba otwarcia aplikacji właściwym hasłem i loginem
    Zakładając że odwiedzę stronę "/profile/"
    Wtedy powinienem być na stronie "/login"
    I kod statusu odpowiedzi powinien być równy 200
    Jeżeli wypełnię pole "_username" wartością "<login>"
    I wypełnię pole "_password" wartością "pass"
    I nacisnę przycisk "_submit"
    Wtedy powinienem być na stronie "/profile/"
    
    Przykłady:
      | login      |
      | colak      |
      | jsiciarek  |
