Set up
------

Informacje o konfiguracji aplikacji ``behat``

Instalacja
==========

Behat instalujemy przez uzupełnienie ``composer.json``

.. literalinclude:: ../../composer.json
    :language: json
    :linenos:
    :emphasize-lines: 3,9-10


Konfiguracja
============

Konfiguracja globalna ``behat.dist.yml``

.. literalinclude:: ../../behat.dist.yml
    :language: yaml
    :linenos:
    :emphasize-lines: 3,9-10

Konfiguracja lokalna ``behat.yml``

.. literalinclude:: ../../behat.yml
    :language: yaml
    :linenos:
    :emphasize-lines: 3,9-10

Struktura katalogu ``features``

.. code-block:: bash

    features
        /attachments
            /file.pdf
        /bootstrap
            /FeatureContext.php

        /user.login.feature

        
