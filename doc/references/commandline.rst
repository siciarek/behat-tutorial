Korzystanie z linii poleceń
---------------------------

Przykładowe User story.

.. code-block:: bash

    $ bin/behat --story-syntax

.. code-block:: yaml

    # language: pl
    [Właściwość|Funkcja|Aspekt|Potrzeba biznesowa]: Internal operations
      In order to stay secret
      As a secret organization
      We need to be able to erase past agents' memory

      Założenia:
        [Zakładając, że|Zakładając|Mając] there is agent A
        [Oraz|I] there is agent B

      Scenariusz: Erasing agent memory
        [Zakładając, że|Zakładając|Mając] there is agent J
        [Oraz|I] there is agent K
        [Jeżeli|Jeśli|Kiedy|Gdy] I erase agent K's memory
        Wtedy there should be agent J
        Ale there should not be agent K

      Szablon scenariusza: Erasing other agents' memory
        [Zakładając, że|Zakładając|Mając] there is agent <agent1>
        [Oraz|I] there is agent <agent2>
        [Jeżeli|Jeśli|Kiedy|Gdy] I erase agent <agent2>'s memory
        Wtedy there should be agent <agent1>
        Ale there should not be agent <agent2>

        Przykłady:
          | agent1 | agent2 |
          | D      | M      |


Lista dostępnych definicji:

.. code-block:: bash

    $ bin/behat --lang=pl --definitions l

Lista dostępnych definicji z opisami:

.. code-block:: bash

    $ bin/behat --lang=pl --definitions i

Powtórne wykonanie tylko tych testów, które ostatnio zakończyły się niepowodzeniem.

.. code-block:: bash

    $ bin/behat --rerun

Wyświetlenie wszystkich opcji konfiguracyjnych.

.. code-block:: bash

    $ bin/behat --config-reference

Uruchomienie testów na sucho.

.. code-block:: bash

    $ bin/behat --dry-run

Zatrzymanie wykonywania po pierwszym nieudanym teście.

.. code-block:: bash

    $ bin/behat --stop-on-failure


